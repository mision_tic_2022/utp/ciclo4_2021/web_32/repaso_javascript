

//Función de orden superior
function ejecutar_operacion( operacion ){
    console.log( operacion() );
}

function sumar(){
    return 5 + 10;
}

function crearOperacion( operador ){
    let operacion;

    if(operador == '+'){
        operacion = (num1, num2) => num1 + num2;
    }else if(operador == '-'){
        operacion = (num1, num2) => num1 - num2;
    }

    return operacion;
}

let operacion = crearOperacion('-');

console.log( operacion(2, 8) );