/*
console.log("Paso 1");

for(let i = 0; i < 10; i++){
    console.log("Ejecutando proceso...");
}

console.log("Paso 2");
*/



//const proceso = 

function proceso() {
    for (let i = 0; i < 10; i++) {
        console.log("Ejecutando proceso...");
    }
}

const promesa = () => new Promise(() => setTimeout(proceso, 2000));
/*function promesa(){
    return new Promise(() => setTimeout(proceso(), 2000));
}
*/
async function ejemplo() {
    console.log("Paso 1");
    await promesa();
    /*promesa().then(() => {
        console.log("Paso 2");
        console.log("Paso 3");
        console.log("Paso 4");
    }
    ).catch(error => {
        console.log("error");
    })
    */
    console.log("Paso 2");
    console.log("Paso 3");
    console.log("Paso 4");
}

//ejemplo();


function saludarFunc(){
    return "Hola mundo desde una Función";
}

const saludar = () => "Hola mundo";

const sumar = (num1, num2) => {
    let resultado = num1 + num2;
    return resultado;
}


saludarFunc();

console.log(saludar());

let resultado = sumar(5, 10);
console.log(resultado);



